package fm.employees.processing.client;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.employees.processing.model.client.request.BatchDepartmentsByIdsRequest;
import fm.employees.processing.model.client.response.DepartmentResponse;
import fm.employees.processing.model.client.response.DepartmentsIdsResponse;
import fm.employees.processing.model.client.response.DepartmentsResponse;
import fm.employees.processing.model.request.DepartmentsSearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "fm-departments-processing-service", url = "${config.feign.fm-departments-processing-service}")
public interface DepartmentsClient {

    @GetMapping("/{department_id}")
    @CommonParams
    ResponseBodyEntity<DepartmentResponse> findDepartmentById(@PathVariable("department_id") int departmentId);

    @PostMapping("/batch/ids")
    @CommonParams
    ResponseBodyEntity<DepartmentsResponse> findDepartmentsByIds(@RequestBody BatchDepartmentsByIdsRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseBodyEntity<DepartmentsIdsResponse> findDepartmentsIds(@RequestBody DepartmentsSearchRequest request);
}
