package fm.employees.processing.client;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.employees.processing.model.client.request.BatchPositionsByIdsRequest;
import fm.employees.processing.model.client.response.PositionResponse;
import fm.employees.processing.model.client.response.PositionsIdsResponse;
import fm.employees.processing.model.client.response.PositionsResponse;
import fm.employees.processing.model.request.PositionsSearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "fm-positions-processing-service", url = "${config.feign.fm-positions-processing-service}")
public interface PositionsClient {

    @GetMapping("/{position_id}")
    @CommonParams
    ResponseBodyEntity<PositionResponse> findPositionById(@PathVariable("position_id") int positionId);

    @PostMapping("/batch")
    @CommonParams
    ResponseBodyEntity<PositionsResponse> findPositionsByIds(@RequestBody BatchPositionsByIdsRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseBodyEntity<PositionsIdsResponse> findPositionsIds(@RequestBody PositionsSearchRequest request);
}
