package fm.employees.processing.client;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.employees.processing.model.client.request.BatchStoresByIdsRequest;
import fm.employees.processing.model.client.response.StoreResponse;
import fm.employees.processing.model.client.response.StoresIdsResponse;
import fm.employees.processing.model.client.response.StoresResponse;
import fm.employees.processing.model.request.StoresSearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "fm-stores-processing-service", url = "${config.feign.fm-stores-processing-service}")

public interface StoresClient {

    @GetMapping("/{store_id}")
    @CommonParams
    ResponseBodyEntity<StoreResponse> findStoreById(@PathVariable("store_id") int storeId);

    @PostMapping("/batch")
    @CommonParams
    ResponseBodyEntity<StoresResponse> findStoresByIds(@RequestBody BatchStoresByIdsRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseBodyEntity<StoresIdsResponse> findStores(@RequestBody StoresSearchRequest request);
}
