package fm.employees.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.employees.processing.controller.doc.EmployeesFullDocs;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeFullResponse;
import fm.employees.processing.model.response.EmployeesFullResponse;
import fm.employees.processing.service.api.EmployeesFullService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EmployeesFullController extends BaseController implements EmployeesFullDocs {

    private final EmployeesFullService employeesFullService;

    @Override
    public ResponseCoreEntity<EmployeeFullResponse> findFullEmployeeById(long employeeId) {
        return response(employeesFullService.findEmployeeById(employeeId));
    }

    @Override
    public ResponseCoreEntity<EmployeesFullResponse> findFullEmployees(SearchRequest searchRequest) {
        return response(employeesFullService.findEmployees(searchRequest));
    }
}
