package fm.employees.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.employees.processing.controller.doc.EmployeesDocs;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeResponse;
import fm.employees.processing.model.response.EmployeesResponse;
import fm.employees.processing.service.api.EmployeesService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EmployeesController extends BaseController implements EmployeesDocs {

    private final EmployeesService employeesService;

    @Override
    public ResponseCoreEntity<EmployeeResponse> findEmployeeById(long employeeId) {
        return response(employeesService.findEmployeeById(employeeId));
    }

    @Override
    public ResponseCoreEntity<EmployeesResponse> findEmployees(SearchRequest searchRequest) {
        return response(employeesService.findEmployees(searchRequest));
    }
}
