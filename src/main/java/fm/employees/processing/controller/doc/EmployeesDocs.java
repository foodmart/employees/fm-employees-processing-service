package fm.employees.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeResponse;
import fm.employees.processing.model.response.EmployeesResponse;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface EmployeesDocs {

    @GetMapping("/{employee_id}")
    @CommonParams
    ResponseCoreEntity<EmployeeResponse> findEmployeeById(@PathVariable("employee_id") long employeeId);

    @PostMapping("/search")
    @CommonParams
    @Pageable
    ResponseCoreEntity<EmployeesResponse> findEmployees(@RequestBody @Valid SearchRequest searchRequest);
}
