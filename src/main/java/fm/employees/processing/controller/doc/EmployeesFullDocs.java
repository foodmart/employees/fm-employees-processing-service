package fm.employees.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeFullResponse;
import fm.employees.processing.model.response.EmployeesFullResponse;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/full")
public interface EmployeesFullDocs {

    @GetMapping("/{employee_id}")
    @CommonParams
    ResponseCoreEntity<EmployeeFullResponse> findFullEmployeeById(@PathVariable("employee_id") long employeeId);

    @PostMapping("/search")
    @CommonParams
    @Pageable
    @CrossOrigin
    ResponseCoreEntity<EmployeesFullResponse> findFullEmployees(@RequestBody @Valid SearchRequest searchRequest);
}
