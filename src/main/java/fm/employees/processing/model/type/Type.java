package fm.employees.processing.model.type;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
public class Type<T> {

    @NotNull
    private T value;

}
