package fm.employees.processing.model.enums;

public enum Gender {
    M, F
}
