package fm.employees.processing.model.entity;

import fm.employees.processing.model.enums.Gender;
import fm.employees.processing.model.enums.MartialStatus;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

import static fm.employees.processing.constant.EmployeeEntityConstants.COLLECTION_NAME;
import static fm.employees.processing.constant.EmployeeEntityConstants.Fields.*;

@Document(collection = COLLECTION_NAME)
@Data
public class Employee {

    @Id
    private String id;

    @Indexed
    @Field(name = EMPLOYEE_ID_FILED_NAME)
    private long employeeId;

    @Field(name = FULL_NAME_FILED_NAME)
    private String fullName;

    @Field(name = FIRST_NAME_FILED_NAME)
    private String firstName;

    @Field(name = LAST_NAME_FILED_NAME)
    private String lastName;

    @Field(name = POSITION_ID_FILED_NAME)
    private int positionId;

    @Field(name = POSITION_TITLE_FILED_NAME)
    private String positionTitle;

    @Field(name = STORE_ID_FILED_NAME)
    private int storeId;

    @Field(name = DEPARTMENT_ID_FILED_NAME)
    private int departmentId;

    @Field(name = BIRTH_DATE_FILED_NAME)
    private LocalDateTime birthDate;

    @Field(name = HIRE_DATE_FILED_NAME)
    private LocalDateTime hireDate;

    @Field(name = END_DATE_FILED_NAME)
    private LocalDateTime endDate;

    @Field(name = SALARY_FILED_NAME)
    private Double salary;

    @Field(name = SUPERVISOR_ID_FILED_NAME)
    private long supervisorId;

    @Field(name = EDUCATIONAL_LEVEL_FILED_NAME)
    private String educationLevel;

    @Field(name = MARTIAL_STATUS_FILED_NAME)
    private MartialStatus maritalStatus;

    @Field(name = GENDER_FILED_NAME)
    private Gender gender;

    @Field(name = MANAGEMENT_ROLE_FILED_NAME)
    private String managementRole;

}
