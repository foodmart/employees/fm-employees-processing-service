package fm.employees.processing.model.client.response;

import fm.employees.processing.model.common.Address;
import fm.employees.processing.model.common.Contact;
import fm.employees.processing.model.common.Regions;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class StoreResponse {

    private int storeId;

    private String storeName;

    private String storeType;

    private int storeNumber;

    private Regions region;

    private Address address;

    private Contact contact;

    private boolean coffeeBar;

    private boolean florist;

    private boolean preparedFood;

    private boolean saladBar;

    private boolean videoStore;

    private LocalDateTime firstOpenedDate;

    private LocalDateTime lastRemodelDate;

    private int frozenSQFT;

    private int grocerySQFT;

    private int meatSQFT;

    private int storeSQFT;


}
