package fm.employees.processing.model.client.response;

import lombok.Data;

@Data
public class PositionResponse {

    private int positionId;

    private String positionTitle;

    private String paymentType;

    private int minScale;

    private int maxScale;

    private String managementRole;

}
