package fm.employees.processing.model.client.response;

import lombok.Data;

@Data
public class DepartmentResponse {

    private long departmentId;

    private String departmentDescription;
}
