package fm.employees.processing.model.common;

import lombok.Getter;

@Getter
public class Regions {

    private long regionId;

    private String salesCity;

    private String salesStateProvince;

    private String salesDistrict;

    private String salesRegion;

    private String salesCountry;

    private String salesDistrictId;
}
