package fm.employees.processing.model.common;

import lombok.Data;

@Data
public class Contact {

    private String storeFax;

    private String storePhone;

}
