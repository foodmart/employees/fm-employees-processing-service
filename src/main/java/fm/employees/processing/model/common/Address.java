package fm.employees.processing.model.common;

import lombok.Data;

@Data
public class Address {

    private String storeCity;

    private String storeCountry;

    private String storeManager;

    private String storePostalCode;

    private String storeState;

    private String storeStreetAddress;
}
