package fm.employees.processing.model.response;

import fm.employees.processing.model.enums.Gender;
import fm.employees.processing.model.enums.MartialStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {

    private long employeeId;

    private String fullName;

    private String firstName;

    private String lastName;

    private int positionId;

    private String positionTitle;

    private int storeId;

    private int departmentId;

    private LocalDateTime birthDate;

    private LocalDateTime hireDate;

    private LocalDateTime endDate;

    private Double salary;

    private long supervisorId;

    private String educationLevel;

    private MartialStatus maritalStatus;

    private Gender gender;

    private String managementRole;
}
