package fm.employees.processing.model.response;

import fm.employees.processing.model.client.response.DepartmentResponse;
import fm.employees.processing.model.client.response.PositionResponse;
import fm.employees.processing.model.client.response.StoreResponse;
import fm.employees.processing.model.common.Supervisor;
import fm.employees.processing.model.enums.Gender;
import fm.employees.processing.model.enums.MartialStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeFullResponse {

    private long employeeId;

    private String fullName;

    private String firstName;

    private String lastName;

    private PositionResponse position;

    private StoreResponse store;

    private DepartmentResponse department;

    private LocalDateTime birthDate;

    private LocalDateTime hireDate;

    private LocalDateTime endDate;

    private Double salary;

    private Supervisor supervisor;

    private String educationLevel;

    private MartialStatus maritalStatus;

    private Gender gender;

    private String managementRole;
}
