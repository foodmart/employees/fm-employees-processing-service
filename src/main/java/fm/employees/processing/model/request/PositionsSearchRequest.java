package fm.employees.processing.model.request;

import fm.employees.processing.model.type.StringType;
import fm.employees.processing.model.type.Type;
import lombok.Data;

@Data
public class PositionsSearchRequest {

    private StringType positionTitle;

    private StringType paymentType;

    private Type<Integer> minScale;

    private Type<Integer> maxScale;

    private StringType managementRole;
}
