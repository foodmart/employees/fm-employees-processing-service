package fm.employees.processing.model.request;

import fm.employees.processing.model.type.StringType;
import fm.employees.processing.model.type.Type;
import lombok.Data;

@Data
public class RegionSearchRequest {

    private StringType salesCity;

    private StringType salesCityProvince;

    private StringType salesDistrict;

    private StringType salesRegions;

    private StringType salesCountry;

    private Type<Integer> salesDistrictId;
}
