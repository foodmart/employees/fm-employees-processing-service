package fm.employees.processing.model.request;

import fm.employees.processing.model.type.StringType;
import lombok.Data;

@Data
public class DepartmentsSearchRequest {

    private StringType departmentDescription;

}
