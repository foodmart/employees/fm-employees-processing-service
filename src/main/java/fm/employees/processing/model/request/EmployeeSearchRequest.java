package fm.employees.processing.model.request;

import fm.employees.processing.model.enums.Gender;
import fm.employees.processing.model.enums.MartialStatus;
import fm.employees.processing.model.type.LocalDateTimeType;
import fm.employees.processing.model.type.StringType;
import fm.employees.processing.model.type.Type;
import jakarta.validation.Valid;
import lombok.Data;

@Data
public class EmployeeSearchRequest {

    private @Valid StringType fullName;

    private @Valid StringType firstName;

    private @Valid StringType lastName;

    private @Valid Type<Integer> positionId;

    private @Valid StringType positionTitle;

    private @Valid Type<Integer> storeId;

    private @Valid Type<Integer> departmentId;

    private @Valid LocalDateTimeType birthDate;

    private @Valid LocalDateTimeType hireDate;

    private @Valid LocalDateTimeType endDate;

    private @Valid Type<Double> salary;

    private @Valid Type<Long> supervisorId;

    private @Valid StringType educationLevel;

    private @Valid Type<MartialStatus> maritalStatus;

    private @Valid Type<Gender> gender;

    private @Valid StringType managementRole;

    /*

     */
}
