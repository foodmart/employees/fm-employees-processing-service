package fm.employees.processing.model.request;

import fm.employees.processing.model.type.StringType;
import lombok.Data;

@Data
public class AddressSearchRequest {

    private StringType storeCity;

    private StringType storeCountry;

    private StringType storeManager;

    private StringType storePostalCode;

    private StringType storeState;

    private StringType storeStreetAddress;
}
