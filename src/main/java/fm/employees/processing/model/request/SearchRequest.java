package fm.employees.processing.model.request;

import jakarta.validation.Valid;
import lombok.Data;


@Data
public class SearchRequest {

    private @Valid EmployeeSearchRequest employee;

    private PositionsSearchRequest position;

    private SupervisorSearchRequest supervisor;

    private DepartmentsSearchRequest department;

    private StoresSearchRequest store;

}
