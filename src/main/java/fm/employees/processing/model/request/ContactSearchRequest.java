package fm.employees.processing.model.request;

import fm.employees.processing.model.type.StringType;
import lombok.Data;

@Data
public class ContactSearchRequest {

    private StringType storeFax;

    private StringType storePhone;

}
