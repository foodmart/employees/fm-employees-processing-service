package fm.employees.processing.model.request;

import lombok.Data;

@Data
public class SupervisorSearchRequest {

    private String fullName;
}
