package fm.employees.processing.repository.api;

import fm.employees.processing.model.entity.Employee;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Optional;

public interface EmployeesRepository {

    Optional<Employee> findEmployeeById(long employeeId);

    List<Employee> findEmployees(Query query);

    List<Employee> findEmployeesByFullname(String fullname);
}
