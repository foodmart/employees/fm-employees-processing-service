package fm.employees.processing.repository.impl;

import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.repository.api.EmployeesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static fm.employees.processing.constant.EmployeeEntityConstants.Fields.EMPLOYEE_ID_FILED_NAME;
import static fm.employees.processing.constant.EmployeeEntityConstants.Fields.FULL_NAME_FILED_NAME;

@RequiredArgsConstructor
@Repository
public class EmployeeRepositoryImpl implements EmployeesRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public Optional<Employee> findEmployeeById(long employeeId) {
        Criteria criteria = Criteria.where(EMPLOYEE_ID_FILED_NAME).is(employeeId);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Employee.class).stream().findFirst();
    }
    @Override
    public List<Employee> findEmployees(Query query) {
        return mongoTemplate.find(query, Employee.class);
    }

    @Override
    public List<Employee> findEmployeesByFullname(String fullname) {
        Criteria criteria = Criteria.where(FULL_NAME_FILED_NAME).regex(fullname);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Employee.class);
    }
}
