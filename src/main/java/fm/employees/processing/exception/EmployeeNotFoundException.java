package fm.employees.processing.exception;

import fm.common.core.exception.ResourcesException;

public class EmployeeNotFoundException extends ResourcesException {

    public EmployeeNotFoundException() {
    }

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
