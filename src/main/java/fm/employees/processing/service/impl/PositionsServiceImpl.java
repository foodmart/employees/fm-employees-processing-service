package fm.employees.processing.service.impl;

import fm.employees.processing.client.PositionsClient;
import fm.employees.processing.model.client.request.BatchPositionsByIdsRequest;
import fm.employees.processing.model.client.response.PositionResponse;
import fm.employees.processing.model.client.response.PositionsResponse;
import fm.employees.processing.model.request.PositionsSearchRequest;
import fm.employees.processing.service.api.PositionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PositionsServiceImpl implements PositionsService {

    private final PositionsClient positionsClient;

    @Override
    public PositionResponse findPositionById(int positionId) {
        return positionsClient.findPositionById(positionId)
                .getData();
    }

    @Override
    public PositionsResponse findPositionsByIds(List<Integer> positionsIds) {
        return positionsClient.findPositionsByIds(BatchPositionsByIdsRequest.builder()
                        .positionsIds(positionsIds)
                        .build())
                .getData();
    }

    @Override
    public List<Integer> findPositionsIds(PositionsSearchRequest request) {
        return positionsClient.findPositionsIds(request)
                .getData()
                .getPositionsIds();
    }


}
