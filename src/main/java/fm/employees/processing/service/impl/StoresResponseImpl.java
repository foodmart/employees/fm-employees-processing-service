package fm.employees.processing.service.impl;

import fm.employees.processing.client.StoresClient;
import fm.employees.processing.model.client.request.BatchStoresByIdsRequest;
import fm.employees.processing.model.client.response.StoreResponse;
import fm.employees.processing.model.client.response.StoresResponse;
import fm.employees.processing.model.request.StoresSearchRequest;
import fm.employees.processing.service.api.StoresService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StoresResponseImpl implements StoresService {

    private final StoresClient storesClient;

    @Override
    public StoreResponse findStoreById(int storeId) {
        return storesClient.findStoreById(storeId)
                .getData();
    }

    @Override
    public StoresResponse findStoresByIds(List<Integer> storesIds) {
        return storesClient.findStoresByIds(BatchStoresByIdsRequest.builder()
                .storesIds(storesIds)
                .build())
                .getData();
    }

    @Override
    public List<Integer> findStores(StoresSearchRequest request) {
        return storesClient.findStores(request)
                .getData()
                .getStoresIds();
    }
}
