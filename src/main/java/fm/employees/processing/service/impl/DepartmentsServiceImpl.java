package fm.employees.processing.service.impl;

import fm.employees.processing.client.DepartmentsClient;
import fm.employees.processing.model.client.request.BatchDepartmentsByIdsRequest;
import fm.employees.processing.model.client.response.DepartmentResponse;
import fm.employees.processing.model.client.response.DepartmentsResponse;
import fm.employees.processing.model.request.DepartmentsSearchRequest;
import fm.employees.processing.service.api.DepartmentsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentsServiceImpl implements DepartmentsService {

    private final DepartmentsClient departmentsClient;

    @Override
    public DepartmentResponse findDepartmentById(int departmentId) {
        return departmentsClient.findDepartmentById(departmentId)
                .getData();
    }

    @Override
    public DepartmentsResponse findDepartmentsByIds(List<Integer> departmentsId) {
        return departmentsClient.findDepartmentsByIds(BatchDepartmentsByIdsRequest.builder()
                        .departmentsIds(departmentsId)
                        .build())
                .getData();
    }

    @Override
    public List<Integer> findDepartmentsIds(DepartmentsSearchRequest request) {
        return departmentsClient.findDepartmentsIds(request)
                .getData()
                .getDepartmentResponses();
    }
}
