package fm.employees.processing.service.impl;

import fm.common.core.api.mapper.MapperProvider;
import fm.common.core.model.header.CommonContext;
import fm.employees.processing.exception.EmployeeNotFoundException;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeResponse;
import fm.employees.processing.model.response.EmployeesResponse;
import fm.employees.processing.repository.api.EmployeesRepository;
import fm.employees.processing.service.api.EmployeesService;
import fm.employees.processing.service.api.QueryBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesRepository employeesRepository;

    private final MapperProvider mapperProvider;

    private final QueryBuilder queryBuilder;

    private final CommonContext commonContext;

    @Override
    public EmployeeResponse findEmployeeById(long employeeId) {
        Optional<Employee> employeeOpt = employeesRepository.findEmployeeById(employeeId);
        if (employeeOpt.isPresent()) {
            return mapperProvider.map(employeeOpt.get(), EmployeeResponse.class);
        } else {
            throw new EmployeeNotFoundException("The requested employee id " + employeeId + " is not found");
        }
    }

    @Override
    public EmployeesResponse findEmployees(SearchRequest request) {
        Query query = queryBuilder.build(request);
        Pageable pageable = PageRequest.of(commonContext.getPage(), commonContext.getSize());
        query = query.with(pageable);
        List<Employee> employees = employeesRepository.findEmployees(query);
        return mapperProvider.map(employees, EmployeesResponse.class);
    }

}
