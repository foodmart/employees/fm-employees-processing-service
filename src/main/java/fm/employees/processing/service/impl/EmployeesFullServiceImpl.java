package fm.employees.processing.service.impl;

import fm.common.core.api.mapper.MapperProvider;
import fm.common.core.model.header.CommonContext;
import fm.employees.processing.exception.EmployeeNotFoundException;
import fm.employees.processing.model.client.response.*;
import fm.employees.processing.model.common.Supervisor;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeFullResponse;
import fm.employees.processing.model.response.EmployeesFullResponse;
import fm.employees.processing.repository.api.EmployeesRepository;
import fm.employees.processing.service.api.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeesFullServiceImpl implements EmployeesFullService {

    private final EmployeesRepository employeesRepository;

    private final DepartmentsService departmentsService;

    private final PositionsService positionsService;

    private final StoresService storesService;

    private final MapperProvider mapperProvider;

    private final QueryBuilder queryBuilder;

    private final CommonContext commonContext;

    @Override
    public EmployeeFullResponse findEmployeeById(long employeeId) {
        Optional<Employee> employeeOpt = employeesRepository.findEmployeeById(employeeId);
        if (employeeOpt.isPresent()) {
            Employee employee = employeeOpt.get();
            DepartmentResponse department = departmentsService.findDepartmentById(employee.getDepartmentId());
            long supervisorId = employee.getSupervisorId();
            Optional<Employee> supervisorOpt = employeesRepository.findEmployeeById(supervisorId);
            PositionResponse position = positionsService.findPositionById(employee.getPositionId());
            StoreResponse store = storesService.findStoreById(employee.getStoreId());
            Supervisor supervisor;
            if (supervisorOpt.isPresent()) {
                supervisor = mapperProvider.map(supervisorOpt.get(), Supervisor.class);
                return mapperProvider.map(employee, department, position, store, supervisor, EmployeeFullResponse.class);
            }
            return mapperProvider.map(employee, department, position, store, Supervisor.class, EmployeeFullResponse.class);
        } else {
            throw new EmployeeNotFoundException("The requested employee id " + employeeId + " is not found");
        }
    }

    @Override
    public EmployeesFullResponse findEmployees(SearchRequest request) {
        Query query = queryBuilder.build(request);
        Pageable pageable = PageRequest.of(commonContext.getPage(), commonContext.getSize());
        query = query.with(pageable);
        List<EmployeeFullResponse> employees = collector(employeesRepository.findEmployees(query));
        return mapperProvider.map(employees, EmployeesFullResponse.class);
    }

    private List<EmployeeFullResponse> collector(List<Employee> employees) {
        if (employees.isEmpty()) return new ArrayList<>();
        DepartmentsResponse departments = departmentsService.findDepartmentsByIds(employees.stream().map(Employee::getDepartmentId).distinct().toList());
        StoresResponse stores = storesService.findStoresByIds(employees.stream().map(Employee::getStoreId).distinct().toList());
        PositionsResponse positions = positionsService.findPositionsByIds(employees.stream().map(Employee::getPositionId).distinct().toList());
        return employees
                .stream()
                .map(employee -> {
                    DepartmentResponse department = departments.getDepartmentResponses()
                            .stream()
                            .filter(departmentResponse -> departmentResponse.getDepartmentId() == employee.getDepartmentId())
                            .findFirst()
                            .orElse(null);
                    StoreResponse store = stores.getStores()
                            .stream()
                            .filter(storeResponse -> storeResponse.getStoreId() == employee.getStoreId())
                            .findFirst()
                            .orElse(null);
                    PositionResponse position = positions.getPositions()
                            .stream()
                            .filter(positionResponse -> positionResponse.getPositionId() == employee.getPositionId())
                            .findFirst()
                            .orElse(null);
                    Optional<Employee> supervisorOpt = employeesRepository.findEmployeeById(employee.getSupervisorId());
                    Supervisor supervisor = new Supervisor();
                    if (supervisorOpt.isPresent()) {
                        supervisor = mapperProvider.map(supervisorOpt.get(), Supervisor.class);
                    }
                    return mapperProvider.map(employee, department, position, store, supervisor, EmployeeFullResponse.class);
                })
                .toList();
    }

}
