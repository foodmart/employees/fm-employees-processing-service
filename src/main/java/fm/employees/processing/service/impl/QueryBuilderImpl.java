package fm.employees.processing.service.impl;

import fm.common.core.model.header.CommonContext;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.request.EmployeeSearchRequest;
import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.type.LocalDateTimeType;
import fm.employees.processing.model.type.StringType;
import fm.employees.processing.model.type.Type;
import fm.employees.processing.repository.api.EmployeesRepository;
import fm.employees.processing.service.api.DepartmentsService;
import fm.employees.processing.service.api.PositionsService;
import fm.employees.processing.service.api.QueryBuilder;
import fm.employees.processing.service.api.StoresService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static fm.employees.processing.constant.EmployeeEntityConstants.Fields.*;

@Service
@RequiredArgsConstructor
public class QueryBuilderImpl implements QueryBuilder {

    private final PositionsService positionsService;

    private final DepartmentsService departmentsService;

    private final StoresService storesService;

    private final EmployeesRepository employeesRepository;

    private final CommonContext commonContext;

    @Override
    public Query build(SearchRequest request) {
        Criteria criteria = new Criteria();
        addCriteriaIds(request, criteria);
        if (request.getEmployee() != null) {
            addEmployeeCriteria(criteria, request.getEmployee());
        }
        return Query.query(criteria);
    }

    private void addCriteriaIds(SearchRequest request, Criteria criteria) {
        List<Integer> positionsIds = new ArrayList<>();
        List<Integer> storesIds = new ArrayList<>();
        List<Integer> departmentsIds = new ArrayList<>();
        List<Long> supervisorIds = new ArrayList<>();
        if (request.getPosition() != null) {
            positionsIds.addAll(positionsService.findPositionsIds(request.getPosition()));
        }
        if (request.getDepartment() != null) {
            departmentsIds.addAll(departmentsService.findDepartmentsIds(request.getDepartment()));
        }
        if (request.getStore() != null) {
            storesIds.addAll(storesService.findStores(request.getStore()));
        }
        if (request.getSupervisor() != null && request.getSupervisor().getFullName() != null) {
            supervisorIds.addAll(employeesRepository.findEmployeesByFullname(request.getSupervisor()
                            .getFullName())
                    .stream()
                    .map(Employee::getEmployeeId)
                    .toList());
        }
        EmployeeSearchRequest employee = request.getEmployee();
        if (employee != null) {
            addSearchRequestId(positionsIds, employee.getPositionId());
            addSearchRequestId(storesIds, employee.getStoreId());
            addSearchRequestId(departmentsIds, employee.getDepartmentId());
            addSearchRequestId(supervisorIds, employee.getSupervisorId());
        }
        if ((request.getEmployee() != null && request.getEmployee().getPositionId() != null) || request.getPosition() != null) {
            criteria.and(POSITION_ID_FILED_NAME).in(positionsIds);
        }
        if ((request.getEmployee() != null && request.getEmployee().getStoreId() != null) || request.getStore() != null) {
            criteria.and(STORE_ID_FILED_NAME).in(storesIds);
        }
        if ((request.getEmployee() != null && request.getEmployee().getDepartmentId() != null) || request.getDepartment() != null) {
            criteria.and(DEPARTMENT_ID_FILED_NAME).in(departmentsIds);
        }
        if ((request.getEmployee() != null && request.getEmployee().getSupervisorId() != null) || request.getSupervisor() != null) {
            criteria.and(SUPERVISOR_ID_FILED_NAME).in(supervisorIds);
        }
    }

    private void addEmployeeCriteria(Criteria criteria, EmployeeSearchRequest employee) {
        addTypeCriteria(criteria, employee.getFullName(), FULL_NAME_FILED_NAME);
        addTypeCriteria(criteria, employee.getFirstName(), FIRST_NAME_FILED_NAME);
        addTypeCriteria(criteria, employee.getLastName(), LAST_NAME_FILED_NAME);
        addTypeCriteria(criteria, employee.getPositionTitle(), POSITION_TITLE_FILED_NAME);
        addTypeCriteria(criteria, employee.getManagementRole(), MANAGEMENT_ROLE_FILED_NAME);
        addTypeCriteria(criteria, employee.getEducationLevel(), EDUCATIONAL_LEVEL_FILED_NAME);
        addTypeCriteria(criteria, employee.getMaritalStatus(), MARTIAL_STATUS_FILED_NAME);
        addTypeCriteria(criteria, employee.getGender(), GENDER_FILED_NAME);
        addTypeCriteria(criteria, employee.getHireDate(), HIRE_DATE_FILED_NAME);
        addTypeCriteria(criteria, employee.getBirthDate(), BIRTH_DATE_FILED_NAME);
        addTypeCriteria(criteria, employee.getEndDate(), END_DATE_FILED_NAME);
        addTypeCriteria(criteria, employee.getSalary(), SALARY_FILED_NAME);
    }

    private void addTypeCriteria(Criteria criteria, Type<?> type, String fieldName) {
        if (type != null) {
            if (type instanceof StringType stringType) {
                if (stringType.isExact()) {
                    criteria.and(fieldName).is(stringType.getValue());
                } else {
                    criteria.and(fieldName).regex(stringType.getValue());
                }
            } else if (type instanceof LocalDateTimeType dateTimeType) {
                criteria.and(fieldName).is(dateTimeType.getValue().atZone(ZoneId.of(commonContext.getTimeZone())).toInstant());
            } else {
                criteria.and(fieldName).is(type.getValue());
            }
        }
    }

    private <T> void addSearchRequestId(List<T> ids, Type<T> type) {
        if (type != null) {
            ids.add(type.getValue());
        }
    }
}
