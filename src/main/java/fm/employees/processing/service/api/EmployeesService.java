package fm.employees.processing.service.api;

import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeResponse;
import fm.employees.processing.model.response.EmployeesResponse;

public interface EmployeesService {

    EmployeeResponse findEmployeeById(long employeeId);

    EmployeesResponse findEmployees(SearchRequest request);

}
