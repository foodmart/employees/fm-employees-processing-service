package fm.employees.processing.service.api;

import fm.employees.processing.model.request.SearchRequest;
import fm.employees.processing.model.response.EmployeeFullResponse;
import fm.employees.processing.model.response.EmployeesFullResponse;

public interface EmployeesFullService {

    EmployeeFullResponse findEmployeeById(long employeeId);

    EmployeesFullResponse findEmployees(SearchRequest request);

}
