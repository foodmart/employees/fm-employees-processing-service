package fm.employees.processing.service.api;

import fm.employees.processing.model.request.SearchRequest;
import org.springframework.data.mongodb.core.query.Query;

public interface QueryBuilder {

    Query build(SearchRequest request);
}
