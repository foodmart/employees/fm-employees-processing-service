package fm.employees.processing.service.api;

import fm.employees.processing.model.client.response.DepartmentResponse;
import fm.employees.processing.model.client.response.DepartmentsResponse;
import fm.employees.processing.model.request.DepartmentsSearchRequest;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface DepartmentsService {

    DepartmentResponse findDepartmentById(int departmentId);

    DepartmentsResponse findDepartmentsByIds(List<Integer> departmentsId);

    List<Integer> findDepartmentsIds(@RequestBody DepartmentsSearchRequest request);

}
