package fm.employees.processing.service.api;

import fm.employees.processing.model.client.response.PositionResponse;
import fm.employees.processing.model.client.response.PositionsResponse;
import fm.employees.processing.model.request.PositionsSearchRequest;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface PositionsService {

    PositionResponse findPositionById(int positionId);

    PositionsResponse findPositionsByIds(List<Integer> positionsIds);

    List<Integer> findPositionsIds(@RequestBody PositionsSearchRequest request);

}
