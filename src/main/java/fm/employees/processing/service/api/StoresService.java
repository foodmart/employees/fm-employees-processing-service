package fm.employees.processing.service.api;


import fm.employees.processing.model.client.response.StoreResponse;
import fm.employees.processing.model.client.response.StoresResponse;
import fm.employees.processing.model.request.StoresSearchRequest;

import java.util.List;

public interface StoresService {

    StoreResponse findStoreById(int storeId);

    StoresResponse findStoresByIds(List<Integer> storesIds);

    List<Integer> findStores(StoresSearchRequest request);

}
