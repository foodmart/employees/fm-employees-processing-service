package fm.employees.processing.mapper;

import fm.common.core.api.mapper.Mapper5;
import fm.employees.processing.model.client.response.DepartmentResponse;
import fm.employees.processing.model.client.response.PositionResponse;
import fm.employees.processing.model.client.response.StoreResponse;
import fm.employees.processing.model.common.Supervisor;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.response.EmployeeFullResponse;
import org.springframework.stereotype.Component;

@Component
public class EmployeeFullResponseMapper implements Mapper5<Employee, DepartmentResponse, PositionResponse, StoreResponse, Supervisor, EmployeeFullResponse> {
    @Override
    public EmployeeFullResponse map(Employee employee, DepartmentResponse departmentResponse, PositionResponse positionResponse, StoreResponse storeResponse, Supervisor supervisor) {
        return EmployeeFullResponse.builder()
                .employeeId(employee.getEmployeeId())
                .fullName(employee.getFullName())
                .gender(employee.getGender())
                .educationLevel(employee.getEducationLevel())
                .lastName(employee.getLastName())
                .managementRole(employee.getManagementRole())
                .maritalStatus(employee.getMaritalStatus())
                .salary(employee.getSalary())
                .store(storeResponse)
                .hireDate(employee.getHireDate())
                .endDate(employee.getEndDate())
                .birthDate(employee.getBirthDate())
                .department(departmentResponse)
                .firstName(employee.getFirstName())
                .position(positionResponse)
                .supervisor(supervisor)
                .build();
    }
}
