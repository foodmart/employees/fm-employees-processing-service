package fm.employees.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.employees.processing.model.response.EmployeeFullResponse;
import fm.employees.processing.model.response.EmployeesFullResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeesFullResponseMapper implements Mapper<List<EmployeeFullResponse>, EmployeesFullResponse> {
    @Override
    public EmployeesFullResponse map(List<EmployeeFullResponse> employeeFullResponses) {
        return EmployeesFullResponse.builder()
                .employees(employeeFullResponses)
                .build();
    }
}
