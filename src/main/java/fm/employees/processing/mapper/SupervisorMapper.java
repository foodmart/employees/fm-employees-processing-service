package fm.employees.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.employees.processing.model.common.Supervisor;
import fm.employees.processing.model.entity.Employee;
import org.springframework.stereotype.Component;

@Component
public class SupervisorMapper implements Mapper<Employee, Supervisor> {
    @Override
    public Supervisor map(Employee employee) {
        return Supervisor.builder()
                .employeeId(employee.getEmployeeId())
                .fullName(employee.getFullName())
                .build();
    }
}
