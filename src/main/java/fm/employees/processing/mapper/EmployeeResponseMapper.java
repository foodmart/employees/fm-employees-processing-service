package fm.employees.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.response.EmployeeResponse;
import org.springframework.stereotype.Component;

@Component
public class EmployeeResponseMapper implements Mapper<Employee, EmployeeResponse> {

    @Override
    public EmployeeResponse map(Employee employee) {
        return EmployeeResponse.builder()
                .employeeId(employee.getEmployeeId())
                .educationLevel(employee.getEducationLevel())
                .departmentId(employee.getDepartmentId())
                .managementRole(employee.getManagementRole())
                .maritalStatus(employee.getMaritalStatus())
                .gender(employee.getGender())
                .positionId(employee.getPositionId())
                .salary(employee.getSalary())
                .endDate(employee.getEndDate())
                .storeId(employee.getStoreId())
                .positionTitle(employee.getPositionTitle())
                .supervisorId(employee.getSupervisorId())
                .lastName(employee.getLastName())
                .fullName(employee.getFullName())
                .hireDate(employee.getHireDate())
                .firstName(employee.getFirstName())
                .birthDate(employee.getBirthDate())
                .build();
    }
}
