package fm.employees.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.employees.processing.model.entity.Employee;
import fm.employees.processing.model.response.EmployeeResponse;
import fm.employees.processing.model.response.EmployeesResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class EmployeesResponseMapper implements Mapper<List<Employee>, EmployeesResponse> {

    private final Mapper<Employee, EmployeeResponse> employeeResponseMapper;

    @Override
    public EmployeesResponse map(List<Employee> employees) {
        return EmployeesResponse.builder()
                .employees(employees.stream().map(employeeResponseMapper::map).toList())
                .build();
    }
}
