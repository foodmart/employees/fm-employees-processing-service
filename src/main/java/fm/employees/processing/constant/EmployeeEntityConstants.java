package fm.employees.processing.constant;

public class EmployeeEntityConstants {

    public static final String COLLECTION_NAME = "employee";

    public static class Fields {

        public static final String EMPLOYEE_ID_FILED_NAME = "employee_id";
        public static final String FULL_NAME_FILED_NAME = "full_name";
        public static final String FIRST_NAME_FILED_NAME = "first_name";
        public static final String LAST_NAME_FILED_NAME = "last_name";
        public static final String POSITION_ID_FILED_NAME = "position_id";
        public static final String POSITION_TITLE_FILED_NAME = "position_title";
        public static final String STORE_ID_FILED_NAME = "store_id";
        public static final String DEPARTMENT_ID_FILED_NAME = "department_id";
        public static final String BIRTH_DATE_FILED_NAME = "birth_date";
        public static final String HIRE_DATE_FILED_NAME = "hire_date";
        public static final String END_DATE_FILED_NAME = "end_date";
        public static final String SALARY_FILED_NAME = "salary";
        public static final String SUPERVISOR_ID_FILED_NAME = "supervisor_id";
        public static final String EDUCATIONAL_LEVEL_FILED_NAME = "education_level";
        public static final String MARTIAL_STATUS_FILED_NAME = "marital_status";
        public static final String GENDER_FILED_NAME = "gender";
        public static final String MANAGEMENT_ROLE_FILED_NAME = "management_role";

    }
}
